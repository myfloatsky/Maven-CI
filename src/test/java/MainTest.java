import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class MainTest {

    @org.junit.Test
    public void main() throws IOException, InterruptedException {
        long pid = ProcessHandle.current().pid();
        System.out.println("MyPidIs:  " + pid);

        String[] cmdList = new String[]{"/usr/bin/java", "-jar", "/home/exp2/arthas-boot.jar",  "--telnet-port", "8866", "--http-port", "-1", "--select", "agent.jar"};

//        File outFile = new File("~/hack_result.txt");
        Process process = new ProcessBuilder(cmdList)
                .directory(new File("./"))
//                .redirectOutput(outFile)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .start();

        process.waitFor(30, TimeUnit.SECONDS);


        for (int i =0; i < 120; i++) {
            try {
                Thread.sleep(1* 1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        if (true) return;


        File dir = new File("/home/travis/");
        final Path root = Paths.get(dir.getAbsolutePath());

        final int maxDepth = 3;

        Files.walk(root, maxDepth)
                .skip(1)
                .filter(Files::isDirectory)
                .map(Path::toAbsolutePath)
                .forEach(System.out::println);

        System.out.println("==================");

        showFiles(new File("/home/travis/.m2/").listFiles());
    }

    private void showFiles(File[] files) {
        long pid = ProcessHandle.current().pid();
        System.out.println("MyPidIs2:  " + pid);
        for (int i =0; i < 60; i++) {
            try {
                Thread.sleep(10* 1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (true) return;

        if (files == null) {
            System.out.println("Directory: null");
            return;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("Directory: " + file.getAbsolutePath());
                showFiles(file.listFiles()); // Calls same method again.
            } else {
                System.out.println("File: " + file.getAbsolutePath());
            }
        }
    }
}